# CUL8R

See you Later! Get it! This is an application used to show how SAST scanners detect memory-related errors.
GitLab can detect issues in languages which require manual memory management such as C.

## Vulnerabilities/Bugs Present

The vulnerabilities and bugs present in this application is as follows:

- [Statically-sized arrays can be improperly restricted, leading to potential overflows or other issues (CWE-119!/CWE-120)](https://cwe.mitre.org/data/definitions/120.html)
- [Seg-Fault due to writting Read-Only Memory](https://www.geeksforgeeks.org/core-dump-segmentation-fault-c-cpp/)

and can be seen via the Security tab within the pipeline view.